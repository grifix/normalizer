<?php

declare(strict_types=1);

namespace Grifix\Normalizer;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Normalizer\DependencyProvider\DependencyProviderInterface;
use Grifix\Normalizer\DependencyProvider\MemoryDependencyProvider;
use Grifix\Normalizer\Exceptions\EmptySchemasException;
use Grifix\Normalizer\Exceptions\NameMeatDataNotFoundException;
use Grifix\Normalizer\Exceptions\NoVersionConverterException;
use Grifix\Normalizer\Exceptions\VersionMetaDataNotFoundException;
use Grifix\Normalizer\ObjectNormalizers\CustomNormalizerWrapper\CustomNormalizerWrapperFactory;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\DefaultObjectNormalizerFactory;
use Grifix\Normalizer\Repository\MemoryNormalizerRepository;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;
use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaAlreadyExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\MemorySchemaRepository;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\SchemaValidator\Repository\SchemaRepositoryInterface;
use Grifix\Normalizer\SchemaValidator\SchemaValidator;
use Grifix\Normalizer\VersionConverter\Repository\Exceptions\VersionConverterAlreadyExistsException;
use Grifix\Normalizer\VersionConverter\Repository\MemoryVersionConverterRepository;
use Grifix\Normalizer\VersionConverter\Repository\VersionConverterRepositoryInterface;
use Grifix\Normalizer\VersionConverter\VersionConverter;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

class Normalizer implements NormalizerInterface
{

    private const META_KEY = '__normalizer__';

    private const META_NAME_KEY = self::META_KEY . '.name';

    private const META_VERSION_KEY = self::META_KEY . '.version';

    public function __construct(
        private readonly NormalizerRepositoryInterface $normalizerRepository,
        private readonly DefaultObjectNormalizerFactory $defaultObjectNormalizerFactory,
        private readonly CustomNormalizerWrapperFactory $customNormalizerWrapperFactory,
        private readonly SchemaRepositoryInterface $jsonSchemaRepository,
        private readonly VersionConverterRepositoryInterface $versionConverterRepository,
        private readonly DependencyProviderInterface $dependencyProvider
    ) {
    }

    public function normalize(object $object): array
    {
        return $this->normalizerRepository->getByObjectClass($object::class)->normalize($object);
    }

    public function denormalize(array $data): object
    {
        return $this->normalizerRepository->getByName(Normalizer::getNormalizerName($data))->denormalize($data);
    }

    /**
     * @param Schema[] $schemas
     * @throws EmptySchemasException
     * @throws NoVersionConverterException
     * @throws EmptySchemasException
     * @throws NoVersionConverterException
     * @throws SchemaAlreadyExistsException
     * @throws VersionConverterAlreadyExistsException
     */
    public function registerCustomObjectNormalizer(
        string $name,
        CustomObjectNormalizerInterface $normalizer,
        array $schemas,
        ?VersionConverterInterface $versionConverter = null
    ): void {
        $version = count($schemas);
        $this->normalizerRepository->add(
            $this->customNormalizerWrapperFactory->createWrapper(
                $name,
                $version,
                $normalizer
            )
        );
        $this->registerJsonSchemas($name, $schemas);
        $this->assertVersionConverter($name, $version, $versionConverter);
        if (null !== $versionConverter) {
            $this->versionConverterRepository->addVersionConverter($versionConverter, $name);
        }
    }

    /**
     * @param Schema[] $schemas
     * @throws EmptySchemasException
     * @throws NoVersionConverterException
     * @throws VersionConverterAlreadyExistsException
     * @throws SchemaAlreadyExistsException
     */
    public function registerDefaultObjectNormalizer(
        string $name,
        string $objectClass,
        array $schemas,
        ?VersionConverterInterface $versionConverter = null,
        array $dependencies = []
    ): void {
        $version = count($schemas);

        $this->registerJsonSchemas($name, $schemas);
        $this->normalizerRepository->add(
            $this->defaultObjectNormalizerFactory->create($name, $objectClass, $version, $dependencies)
        );
        $this->assertVersionConverter($name, $version, $versionConverter);
        if (null !== $versionConverter) {
            $this->versionConverterRepository->addVersionConverter($versionConverter, $name);
        }
    }

    /**
     * @param Schema[] $schemas
     * @throws EmptySchemasException
     * @throws SchemaAlreadyExistsException
     */
    private function registerJsonSchemas(string $normalizerName, array $schemas): void
    {
        if (count($schemas) === 0) {
            throw new EmptySchemasException($normalizerName);
        }
        foreach ($schemas as $i => $schema) {
            $this->jsonSchemaRepository->add($normalizerName, $i + 1, $schema);
        }
    }

    public function registerDependency(object $dependency): void
    {
        $this->dependencyProvider->set($dependency);
    }

    public function hasNormalizerWithName(string $normalizerName): bool
    {
        return $this->normalizerRepository->hasWithName($normalizerName);
    }

    public function hasNormalizerWithObjectClass(string $objectClass): bool
    {
        return $this->normalizerRepository->hasWithObjectClass($objectClass);
    }

    private function assertVersionConverter(
        string $normalizerName,
        int $version,
        ?VersionConverterInterface $versionConverter
    ): void {
        if ($version === 1) {
            return;
        }
        if (null === $versionConverter) {
            throw new NoVersionConverterException($normalizerName);
        }
    }

    /**
     * @internal
     */
    public static function getNormalizerName(array $data): string
    {
        $wrapper = ArrayWrapper::create($data);
        if (!$wrapper->hasElement(self::META_NAME_KEY)) {
            throw new NameMeatDataNotFoundException();
        }
        return $wrapper->getElement(self::META_NAME_KEY);
    }

    /**
     * @internal
     */
    public static function setNormalizerName(array $data, string $normalizerName): array
    {
        $wrapper = ArrayWrapper::create($data);
        $wrapper->setElement(self::META_NAME_KEY, $normalizerName);
        return $wrapper->getWrapped();
    }

    /**
     * @internal
     */
    public static function setNormalizerVersion(array $data, int $normalizerVersion): array
    {
        $wrapper = ArrayWrapper::create($data);
        $wrapper->setElement(self::META_VERSION_KEY, $normalizerVersion);
        return $wrapper->getWrapped();
    }

    /**
     * @internal
     */
    public static function getNormalizerVersion(array $data): int
    {
        $wrapper = ArrayWrapper::create($data);
        if (!$wrapper->hasElement(self::META_VERSION_KEY)) {
            throw new VersionMetaDataNotFoundException();
        }
        return $wrapper->getElement(self::META_VERSION_KEY);
    }

    /**
     * @internal
     */
    public static function hasNormalizerData(array $data): bool
    {
        return ArrayWrapper::create($data)->hasElement(self::META_KEY);
    }

    /**
     * @internal
     */
    public static function removeNormalizerData(array $data): array
    {
        $wrapper = ArrayWrapper::create($data);
        $wrapper->removeElement(self::META_KEY);
        return $wrapper->getWrapped();
    }

    public static function create(): NormalizerInterface
    {
        $normalizerRepository = new MemoryNormalizerRepository();
        $versionConverterRepository = new MemoryVersionConverterRepository();
        $versionConverter = new VersionConverter($versionConverterRepository);
        $jsonSchemaRepository = new MemorySchemaRepository();
        $dependencyProvider = new MemoryDependencyProvider();
        $jsonSchemaValidator = new SchemaValidator($jsonSchemaRepository);
        $customNormalizerWrapperFactory = new CustomNormalizerWrapperFactory(
            $jsonSchemaValidator,
            $versionConverter
        );
        $defaultObjectNormalizerFactory = new DefaultObjectNormalizerFactory(
            $normalizerRepository,
            $dependencyProvider,
            $jsonSchemaValidator,
            $versionConverter,
            $jsonSchemaRepository
        );

        return new Normalizer(
            $normalizerRepository,
            $defaultObjectNormalizerFactory,
            $customNormalizerWrapperFactory,
            $jsonSchemaRepository,
            $versionConverterRepository,
            $dependencyProvider
        );
    }
}
