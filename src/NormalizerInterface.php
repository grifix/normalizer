<?php

declare(strict_types=1);

namespace Grifix\Normalizer;

use Grifix\Normalizer\Exceptions\EmptySchemasException;
use Grifix\Normalizer\Exceptions\NoVersionConverterException;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\Repository\Exceptions\NormalizerDoesNotExistException;
use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaAlreadyExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\VersionConverter\Repository\Exceptions\VersionConverterAlreadyExistsException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

interface NormalizerInterface
{
    /**
     * @throws NormalizerDoesNotExistException
     */
    public function normalize(object $object): array;

    /**
     * @throws NormalizerDoesNotExistException
     */
    public function denormalize(array $data): object;

    /**
     * @param Schema[] $schemas
     * @throws EmptySchemasException
     * @throws NoVersionConverterException
     * @throws EmptySchemasException
     * @throws NoVersionConverterException
     * @throws SchemaAlreadyExistsException
     * @throws VersionConverterAlreadyExistsException
     */
    public function registerCustomObjectNormalizer(
        string $name,
        CustomObjectNormalizerInterface $normalizer,
        array $schemas,
        ?VersionConverterInterface $versionConverter = null,
    ): void;

    /**
     * @param Schema[] $schemas
     * @throws EmptySchemasException
     * @throws NoVersionConverterException
     * @throws VersionConverterAlreadyExistsException
     * @throws SchemaAlreadyExistsException
     */
    public function registerDefaultObjectNormalizer(
        string $name,
        string $objectClass,
        array $schemas,
        ?VersionConverterInterface $versionConverter = null,
        array $dependencies = []
    ): void;

    public function registerDependency(object $dependency): void;

    public function hasNormalizerWithName(string $normalizerName): bool;

    public function hasNormalizerWithObjectClass(string $objectClass): bool;
}
