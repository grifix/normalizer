<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository;

use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaAlreadyExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaDoesNotExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;

interface SchemaRepositoryInterface
{
    /**
     * @throws SchemaAlreadyExistsException
     */
    public function add(string $normalizerName, int $version, Schema $schema): void;

    public function has(string $normalizerName, int $version): bool;

    /**
     * @throws SchemaDoesNotExistsException
     */
    public function get(string $normalizerName, int $version): Schema;
}
