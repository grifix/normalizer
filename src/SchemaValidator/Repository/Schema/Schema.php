<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema;

use Exception;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\ArrayOfObjectsProperty;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\MixedProperty;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\ObjectProperty;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\PropertyInterface;
use JetBrains\PhpStorm\ArrayShape;

final class Schema
{
    /** @var PropertyInterface[] */
    private array $properties = [];

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function withObjectProperty(string $name, array $allowedNormalizers, bool $nullable = false): self
    {
        $this->properties[] = ObjectProperty::create($name, $allowedNormalizers, $nullable);
        return $this;
    }

    public function withArrayOfObjectsProperty(string $name, array $allowedNormalizers): self
    {
        $this->properties[] = ArrayOfObjectsProperty::create($name, $allowedNormalizers);
        return $this;
    }

    public function withStringProperty(string $name, bool $nullable = false): self
    {
        $this->properties[] = MixedProperty::createString($name, $nullable);
        return $this;
    }

    public function withArrayProperty(string $name, bool $nullable = false): self
    {
        $this->properties[] = MixedProperty::createArray($name, $nullable);
        return $this;
    }

    public function withBooleanProperty(string $name, bool $nullable = false): self
    {
        $this->properties[] = MixedProperty::createBoolean($name, $nullable);
        return $this;
    }

    public function withIntegerProperty(string $name, bool $nullable = false): self
    {
        $this->properties[] = MixedProperty::createInteger($name, $nullable);
        return $this;
    }

    public function withNumberProperty(string $name, bool $nullable = false): self
    {
        $this->properties[] = MixedProperty::createNumber($name, $nullable);
        return $this;
    }

    public function withMixedObjectProperty(string $name, bool $nullable = false): self
    {
        $this->properties[] = MixedProperty::createObject($name, $nullable);
        return $this;
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function getPropertyAllowedNormalizers(string $propertyName): array
    {
        return $this->getProperty($propertyName)->getAllowedNormalizers();
    }

    private function getProperty(string $propertyName): PropertyInterface
    {
        foreach ($this->properties as $property) {
            if ($property->getName() === $propertyName) {
                return $property;
            }
        }
        throw new Exception(
            sprintf(
                'Property [%s] does not exist in schema]',
                $propertyName,
            )
        );
    }

    #[ArrayShape([
        'type' => "string",
        'additionalProperties' => "false",
        'required' => "array",
        'properties' => "array"
    ])] public function toJsonSchema(): array
    {
        $result = [
            'type' => 'object',
            'additionalProperties' => false,
            'required' => [],
            'properties' => []
        ];

        foreach ($this->properties as $property) {
            $result['required'][] = $property->getName();
            $result['properties'][$property->getName()] = $property->toSchema();
        }

        return $result;
    }
}
