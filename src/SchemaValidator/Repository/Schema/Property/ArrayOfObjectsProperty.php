<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema\Property;

use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\Exceptions\AllowedNormalizersCannotBeEmptyException;

final class ArrayOfObjectsProperty implements PropertyInterface
{

    private function __construct(
        private readonly string $name,
        private readonly array $allowedNormalizers
    ) {
        if (empty($allowedNormalizers)) {
            throw new AllowedNormalizersCannotBeEmptyException($name);
        }
    }

    public static function create(string $name, array $allowedNormalizers): self
    {
        return new self($name, $allowedNormalizers);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAllowedNormalizers(): array
    {
        return $this->allowedNormalizers;
    }

    public function toSchema(): array|string
    {
        return 'array';
    }
}
