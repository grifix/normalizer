<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema\Property;

use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\Exceptions\AllowedNormalizersCannotBeEmptyException;

final class ObjectProperty implements PropertyInterface
{

    /**
     * @param string[] $allowedNormalizers
     */
    private function __construct(
        private readonly string $name,
        private readonly array $allowedNormalizers,
        private readonly bool $nullable = false,
    ) {
        if (empty($allowedNormalizers)) {
            throw new AllowedNormalizersCannotBeEmptyException($name);
        }
    }

    /**
     * @param string[] $allowedNormalizers
     */
    public static function create(string $name, array $allowedNormalizers, bool $nullable = false): self
    {
        return new self($name, $allowedNormalizers, $nullable);
    }

    public function toSchema(): array|string
    {
        if ($this->nullable) {
            return [
                'object',
                'null'
            ];
        }
        return 'object';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAllowedNormalizers(): array
    {
        return $this->allowedNormalizers;
    }
}
