<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema\Property;

use Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\Exceptions\InvalidScalarPropertyException;

final class MixedProperty implements PropertyInterface
{
    private const TYPE_ARRAY = 'array';
    private const TYPE_STRING = 'string';
    private const TYPE_NUMBER = 'number';
    private const TYPE_INTEGER = 'integer';
    private const TYPE_BOOLEAN = 'boolean';
    private const TYPE_OBJECT = 'object';
    private const VALID_TYPES = [
        self::TYPE_ARRAY,
        self::TYPE_STRING,
        self::TYPE_NUMBER,
        self::TYPE_INTEGER,
        self::TYPE_BOOLEAN,
        self::TYPE_OBJECT
    ];

    private function __construct(
        private readonly string $name,
        private readonly string $type,
        private readonly bool $nullable = false
    ) {
        if (!(in_array($this->type, self::VALID_TYPES))) {
            throw new InvalidScalarPropertyException($name);
        }
    }

    public function toSchema(): string|array
    {
        if ($this->nullable) {
            return ['type' =>['null', $this->type]];
        }
        return ['type' => $this->type];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public static function createString(string $name, bool $nullable = false): self
    {
        return new self($name, self::TYPE_STRING, $nullable);
    }

    public static function createArray(string $name, bool $nullable = false): self
    {
        return new self($name, self::TYPE_ARRAY, $nullable);
    }

    public static function createBoolean(string $name, bool $nullable = false): self
    {
        return new self($name, self::TYPE_BOOLEAN, $nullable);
    }

    public static function createNumber(string $name, bool $nullable = false): self
    {
        return new self($name, self::TYPE_NUMBER, $nullable);
    }

    public static function createInteger(string $name, bool $nullable = false): self
    {
        return new self($name, self::TYPE_INTEGER, $nullable);
    }

    public static function createObject(string $name, bool $nullable = false): self
    {
        return new self($name, self::TYPE_OBJECT, $nullable);
    }

    public function getAllowedNormalizers(): array
    {
        return [];
    }
}
