<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema\Property;

interface PropertyInterface
{
    public function toSchema(): array|string;

    public function getName(): string;

    public function getAllowedNormalizers(): array;
}
