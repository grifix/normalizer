<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\Exceptions;

final class InvalidScalarPropertyException extends \Exception
{

    public function __construct(string $name)
    {
        parent::__construct(sprintf('Invalid scalar property [%s]', $name));
    }
}
