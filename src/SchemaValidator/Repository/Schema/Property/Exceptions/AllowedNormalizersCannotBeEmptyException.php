<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Schema\Property\Exceptions;

use Exception;

final class AllowedNormalizersCannotBeEmptyException extends Exception
{

    public function __construct(string $propertyName)
    {
        parent::__construct(sprintf('Allowed normalizers cannot be empty for property [%s]!', $propertyName));
    }
}
