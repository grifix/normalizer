<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaAlreadyExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaDoesNotExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;

final class MemorySchemaRepository implements SchemaRepositoryInterface
{
    private ArrayWrapper $jsonSchemas;

    public function __construct()
    {
        $this->jsonSchemas = ArrayWrapper::create();
    }


    public function add(string $normalizerName, int $version, Schema $schema): void
    {
        $pathKey = $this->createPathKey($normalizerName, $version);
        if ($this->jsonSchemas->hasElement($pathKey)) {
            throw new SchemaAlreadyExistsException($normalizerName, $version);
        }
        $this->jsonSchemas->setElement($pathKey, $schema);
    }

    public function has(string $normalizerName, int $version): bool
    {
        return $this->jsonSchemas->hasElement($this->createPathKey($normalizerName, $version));
    }

    public function get(string $normalizerName, int $version): Schema
    {
        if (!$this->has($normalizerName, $version)) {
            throw new SchemaDoesNotExistsException($normalizerName, $version);
        }
        return $this->jsonSchemas->getElement($this->createPathKey($normalizerName, $version));
    }

    private function createPathKey(string $normalizerName, int $version): string
    {
        return $normalizerName . '.' . $version;
    }

}
