<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Exceptions;

final class SchemaDoesNotExistsException extends \Exception
{

    public function __construct(string $normalizerName, int $version)
    {
        parent::__construct(
            sprintf('Schema does not exist for normalizer [%s] in version [%s]!', $normalizerName, $version)
        );
    }
}
