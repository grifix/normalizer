<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Repository\Exceptions;

final class SchemaAlreadyExistsException extends \Exception
{
    public function __construct(string $normalizerName, int $version)
    {
        parent::__construct(
            sprintf('Schema already exists for normalizer [%s] in version [%s]!', $normalizerName, $version)
        );
    }
}
