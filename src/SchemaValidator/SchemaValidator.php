<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator;

use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\SchemaValidator\Exceptions\InvalidDataException;
use Grifix\Normalizer\SchemaValidator\Repository\SchemaRepositoryInterface;
use JsonSchema\Validator;

final class SchemaValidator
{
    private Validator $validator;

    public function __construct(
        private readonly SchemaRepositoryInterface $repository,
    ) {
        $this->validator = new Validator();
    }

    public function validate(array $data): void
    {
        $normalizerName = Normalizer::getNormalizerName($data);
        $version = Normalizer::getNormalizerVersion($data);

        $schema = $this->repository->get($normalizerName, $version);

        $data = Normalizer::removeNormalizerData($data);

        $json = json_encode($data);
        $json = json_decode($json);


        $this->validator->validate($json, $schema->toJsonSchema());
        if (!$this->validator->isValid()) {
            throw new InvalidDataException($normalizerName, $version, $this->validator->getErrors());
        }
    }
}
