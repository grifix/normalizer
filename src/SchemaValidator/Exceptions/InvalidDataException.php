<?php

declare(strict_types=1);

namespace Grifix\Normalizer\SchemaValidator\Exceptions;

use Exception;

final class InvalidDataException extends Exception
{

    public function __construct(
        public readonly string $normalizerName,
        public readonly int $version,
        public readonly array $errors
    ) {
        parent::__construct(sprintf('Invalid data for normalizer [%s] version [%s]!', $normalizerName, $version));
    }
}
