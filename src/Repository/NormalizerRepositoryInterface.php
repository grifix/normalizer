<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Repository;

use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;

interface NormalizerRepositoryInterface
{
    public function add(ObjectNormalizerInterface $newNormalizer): void;

    public function getByObjectClass(string $objectClass): ObjectNormalizerInterface;

    public function getByName(string $name): ObjectNormalizerInterface;

    public function hasWithName(string $name): bool;

    public function hasWithObjectClass(string $objectClass): bool;

    /**
     * @return ObjectNormalizerInterface[]
     */
    public function getByNames(array $names): array;
}
