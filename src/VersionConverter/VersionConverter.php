<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter;

use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\VersionConverter\Repository\VersionConverterRepositoryInterface;

final class VersionConverter
{

    public function __construct(private readonly VersionConverterRepositoryInterface $versionConverterRepository)
    {
    }

    public function convert(array $data, int $toVersion): array
    {
        $normalizerName = Normalizer::getNormalizerName($data);
        while (normalizer::getNormalizerVersion($data) !== $toVersion) {
            $version = Normalizer::getNormalizerVersion($data);
            $data = $this->versionConverterRepository->getVersionConverter($normalizerName)
                ->convert($data, $version, Normalizer::getNormalizerName($data));
            $data = Normalizer::setNormalizerVersion($data, $version +1);
        }
        return $data;
    }
}
