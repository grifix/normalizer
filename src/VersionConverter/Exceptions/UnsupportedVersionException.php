<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter\Exceptions;

final class UnsupportedVersionException extends \Exception
{

    public function __construct(string $normalizerName, int $version)
    {
        parent::__construct(
            sprintf(
                'Version converter for [%s] normalizer does not support version [%s]',
                $normalizerName,
                $version
            )
        );
    }
}
