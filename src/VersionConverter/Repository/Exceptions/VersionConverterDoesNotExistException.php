<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter\Repository\Exceptions;

final class VersionConverterDoesNotExistException extends \Exception
{

    public function __construct(string $normalizerName)
    {
        parent::__construct(sprintf('Version converter for normalizer [%s] does not exist!', $normalizerName));
    }
}
