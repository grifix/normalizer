<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter\Repository\Exceptions;

use Exception;

final class VersionConverterAlreadyExistsException extends Exception
{

    public function __construct(string $normalizerName)
    {
        parent::__construct(sprintf('Version converter for normalizer [%s] already exists!', $normalizerName));
    }
}
