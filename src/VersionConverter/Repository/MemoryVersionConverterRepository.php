<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter\Repository;

use Grifix\Normalizer\VersionConverter\Repository\Exceptions\VersionConverterAlreadyExistsException;
use Grifix\Normalizer\VersionConverter\Repository\Exceptions\VersionConverterDoesNotExistException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

final class MemoryVersionConverterRepository implements VersionConverterRepositoryInterface
{
    /** @var VersionConverterInterface[] */
    private array $converters = [];

    public function addVersionConverter(VersionConverterInterface $versionConverter, string $normalizerName): void
    {
        foreach ($this->converters as $name => $value) {
            if ($name === $normalizerName) {
                throw new VersionConverterAlreadyExistsException($normalizerName);
            }
        }
        $this->converters[$normalizerName] = $versionConverter;
    }

    public function getVersionConverter(string $normalizerName): VersionConverterInterface
    {
        foreach ($this->converters as $name => $converter) {
            if ($name === $normalizerName) {
                return $converter;
            }
        }
        throw new VersionConverterDoesNotExistException($normalizerName);
    }
}
