<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter\Repository;

use Grifix\Normalizer\VersionConverter\Repository\Exceptions\VersionConverterAlreadyExistsException;
use Grifix\Normalizer\VersionConverter\Repository\Exceptions\VersionConverterDoesNotExistException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

interface VersionConverterRepositoryInterface
{
    /**
     * @throws VersionConverterAlreadyExistsException
     */
    public function addVersionConverter(VersionConverterInterface $versionConverter, string $normalizerName): void;

    /**
     * @throws VersionConverterDoesNotExistException
     */
    public function getVersionConverter(string $normalizerName): VersionConverterInterface;
}
