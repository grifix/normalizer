<?php

declare(strict_types=1);

namespace Grifix\Normalizer\VersionConverter;

use Grifix\Normalizer\VersionConverter\Exceptions\UnsupportedVersionException;

interface VersionConverterInterface
{
    /**
     * @throws UnsupportedVersionException
     */
    public function convert(array $data, int $dataVersion, string $normalizerName): array;
}
