<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\CustomNormalizerWrapper;

use Grifix\Normalizer\SchemaValidator\SchemaValidator;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\VersionConverter\VersionConverter;

final class CustomNormalizerWrapperFactory
{
    public function __construct(
        private readonly SchemaValidator $jsonSchemaValidator,
        private readonly VersionConverter $versionConverter
    ) {
    }

    public function createWrapper(
        string $name,
        int $version,
        CustomObjectNormalizerInterface $normalizer
    ): CustomNormalizerWrapper
    {
        return new CustomNormalizerWrapper(
            $name,
            $version,
            $normalizer,
            $this->jsonSchemaValidator,
            $this->versionConverter
        );
    }
}
