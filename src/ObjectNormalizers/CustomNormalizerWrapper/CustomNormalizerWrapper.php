<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\CustomNormalizerWrapper;

use Grifix\Normalizer\SchemaValidator\SchemaValidator;
use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\VersionConverter\VersionConverter;

final class CustomNormalizerWrapper implements ObjectNormalizerInterface
{

    public function __construct(
        private readonly string $name,
        private readonly int $version,
        private readonly CustomObjectNormalizerInterface $normalizer,
        private readonly SchemaValidator $jsonSchemaValidator,
        private readonly VersionConverter $versionConverter
    ) {
    }

    public function normalize(object $object): array
    {
        $result = $this->normalizer->normalize($object);
        $result = Normalizer::setNormalizerName($result, $this->getName());
        $result = Normalizer::setNormalizerVersion($result, $this->getVersion());
        $this->jsonSchemaValidator->validate($result);
        return $result;
    }

    public function denormalize(array $data): object
    {
        $this->jsonSchemaValidator->validate($data);
        if ($this->getVersion() > 1) {
            $data = $this->versionConverter->convert($data, $this->getVersion());
        }
        return $this->normalizer->denormalize($data);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getObjectClass(): string
    {
        return $this->normalizer->getObjectClass();
    }

    public function getVersion(): int
    {
        return $this->version;
    }
}
