<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers;

use DateTimeInterface;
use Grifix\Normalizer\ObjectNormalizers\Exceptions\InvalidObjectTypeException;

final class DateTimeNormalizer implements CustomObjectNormalizerInterface
{
    public function normalize(object $object): array
    {
        if ( ! ($object instanceof \DateTime)) {
            throw new InvalidObjectTypeException($object::class, \DateTime::class);
        }

        return [
            'value' => $object->format(DateTimeInterface::ATOM)
        ];
    }

    public function denormalize(array $data): object
    {
        return new \DateTime($data['value']);
    }

    public function getObjectClass(): string
    {
        return \DateTime::class;
    }
}
