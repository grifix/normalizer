<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\Exceptions;

use Exception;

final class InvalidValueClassException extends Exception
{
    public function __construct(
        string $objectClass,
        string $propertyName,
        array $allowedTypes,
        string $actualClass
    ) {
        parent::__construct(
            sprintf(
                'Invalid value type for property [%s::%s]! Allowed types are [%s] but [%s] given!',
                $objectClass,
                $propertyName,
                implode(', ', $allowedTypes),
                $actualClass
            )
        );
    }
}
