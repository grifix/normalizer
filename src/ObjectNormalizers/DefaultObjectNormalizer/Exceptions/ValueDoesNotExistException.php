<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\Exceptions;

final class ValueDoesNotExistException extends \Exception
{

    public function __construct(string $class, string $property)
    {
        parent::__construct(
            sprintf(
                'Cannot denormalize class [%s] there is no value for [%s] property in normalized array!',
                $class,
                $property
            )
        );
    }
}
