<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer;

use Grifix\Normalizer\DependencyProvider\DependencyProviderInterface;
use Grifix\Normalizer\SchemaValidator\SchemaValidator;
use Grifix\Normalizer\SchemaValidator\Repository\SchemaRepositoryInterface;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;
use Grifix\Normalizer\VersionConverter\VersionConverter;

final class DefaultObjectNormalizerFactory
{

    public function __construct(
        private readonly NormalizerRepositoryInterface $normalizerRepository,
        private readonly DependencyProviderInterface $dependencyProvider,
        private readonly SchemaValidator $jsonSchemaValidator,
        private readonly VersionConverter $versionConverter,
        private readonly SchemaRepositoryInterface $jsonSchemaRepository,
    ) {
    }

    public function create(
        string $name,
        string $objectClass,
        int $version = 1,
        array $dependencies = []
    ): DefaultObjectNormalizer {
        return new DefaultObjectNormalizer(
            $name,
            $objectClass,
            $this->normalizerRepository,
            $this->dependencyProvider,
            $this->versionConverter,
            $this->jsonSchemaRepository,
            $this->jsonSchemaValidator,
            $dependencies,
            $version
        );
    }
}
