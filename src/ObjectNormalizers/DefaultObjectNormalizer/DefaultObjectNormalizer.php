<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer;

use Grifix\Normalizer\DependencyProvider\DependencyProviderInterface;
use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\Exceptions\InvalidValueClassException;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\Exceptions\ValueDoesNotExistException;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\SchemaValidator\Repository\SchemaRepositoryInterface;
use Grifix\Normalizer\SchemaValidator\SchemaValidator;
use Grifix\Normalizer\VersionConverter\VersionConverter;
use Grifix\Reflection\ReflectionObject;

final class DefaultObjectNormalizer implements ObjectNormalizerInterface
{

    private Schema $schema;

    public function __construct(
        private readonly string $name,
        private readonly string $objectClass,
        private readonly NormalizerRepositoryInterface $normalizerRepository,
        private readonly DependencyProviderInterface $dependencyProvider,
        private readonly VersionConverter $versionConverter,
        private readonly SchemaRepositoryInterface $jsonSchemaRepository,
        private readonly SchemaValidator $jsonSchemaValidator,
        private readonly array $dependencies = [],
        private readonly int $version = 1,
    ) {
        $this->schema = $this->jsonSchemaRepository->get($this->name, $this->version);
    }

    public function normalize(object $object): array
    {
        return $this->normalizeValue($object);
    }

    public function denormalize(array $data): object
    {
        return $this->denormalizeValue($data);
    }

    private function denormalizeValue(mixed $value): mixed
    {
        if (is_array($value)) {
            if (Normalizer::hasNormalizerData($value)) {
                return $this->denormalizeObject($value);
            }

            return $this->denormalizeArray($value);
        }

        return $value;
    }

    private function denormalizeArray(array $array): array
    {
        $result = [];
        foreach ($array as $k => $v) {
            $result[$k] = $this->denormalizeValue($v);
        }

        return $result;
    }

    public function denormalizeByOtherNormalizer(string $normalizerName, array $data): object
    {
        return $this->normalizerRepository->getByName($normalizerName)->denormalize($data);
    }

    private function denormalizeObject(array $data): object
    {
        $name = Normalizer::getNormalizerName($data);
        if ($name !== $this->name) {
            return $this->denormalizeByOtherNormalizer($name, $data);
        }

        $this->jsonSchemaValidator->validate($data);
        if ($this->getVersion() > 1) {
            $data = $this->versionConverter->convert($data, $this->getVersion());
        }


        $reflectionClass = new \ReflectionClass($this->objectClass);
        $result = $reflectionClass->newInstanceWithoutConstructor();
        $reflectionObject = new ReflectionObject($result);
        foreach ($reflectionClass->getProperties() as $property) {
            if ($this->isDependency($property->getName())) {
                $reflectionObject->setPropertyValue(
                    $property->getName(),
                    $this->dependencyProvider->get($property->getType()->getName())
                );
                continue;
            }
            if (!array_key_exists($property->getName(), $data)) {
                throw new ValueDoesNotExistException($this->objectClass, $property->getName());
            }
            $reflectionObject->setPropertyValue(
                $property->getName(),
                $this->denormalizeValue($data[$property->getName()])
            );
        }

        return $result;
    }

    private function normalizeValue(mixed $value, ?string $propertyName = null): mixed
    {
        if (is_object($value)) {
            return $this->normalizeObject($value, $propertyName);
        }

        if (is_array($value)) {
            return $this->normalizeArray($value, $propertyName);
        }

        return $value;
    }

    private function normalizeArray(array $array, ?string $propertyName = null): array
    {
        if ($propertyName) {
            $this->checkPropertyType($propertyName, $array);
        }

        $result = [];
        foreach ($array as $key => $value) {
            $result[$key] = $this->normalizeValue($value);
        }

        return $result;
    }

    private function normalizeByOtherNormalizer(object $object): array
    {
        return $this->normalizerRepository->getByObjectClass($object::class)->normalize($object);
    }

    private function normalizeObject(object $object, ?string $propertyName = null): array
    {
        if ($propertyName) {
            $this->checkPropertyType($propertyName, $object);
        }

        if ($object::class !== $this->objectClass) {
            return $this->normalizeByOtherNormalizer($object);
        }

        $result = [];

        $reflectionObject = new ReflectionObject($object);
        foreach ($reflectionObject->getPropertyValues() as $key => $value) {
            if ($this->isDependency($key)) {
                continue;
            }
            $result[$key] = $this->normalizeValue($value, $key);
        }
        $result = Normalizer::setNormalizerName($result, $this->name);
        $result = Normalizer::setNormalizerVersion($result, $this->version);
        $this->jsonSchemaValidator->validate($result);
        return $result;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getObjectClass(): string
    {
        return $this->objectClass;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    private function isDependency(string $propertyName): bool
    {
        return in_array($propertyName, $this->dependencies);
    }

    private function checkPropertyType(string $propertyName, mixed $value): void
    {
        $allowedTypes = $this->fetchAllowedTypes($propertyName);
        if (empty($allowedTypes)) {
            return;
        }
        if (is_array($value)) {
            foreach ($value as $val) {
                $this->checkPropertyType($propertyName, $val);
            }
            return;
        }
        if (null === $value) {
            return;
        }

        if (!in_array($value::class, $allowedTypes)) {
            throw new InvalidValueClassException($this->objectClass, $propertyName, $allowedTypes, $value::class);
        }
    }

    private function fetchAllowedTypes(string $propertyName): array
    {
        $allowedNormalizerNames = $this->schema->getPropertyAllowedNormalizers($propertyName);
        if (empty($allowedNormalizerNames)) {
            return [];
        }

        $allowedNormalizers = $this->normalizerRepository->getByNames($allowedNormalizerNames);

        $result = [];
        foreach ($allowedNormalizers as $normalizer) {
            $result[] = $normalizer->getObjectClass();
        }
        return $result;
    }
}
