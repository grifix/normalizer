<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers;

use DateTimeInterface;
use Grifix\Normalizer\ObjectNormalizers\Exceptions\InvalidObjectTypeException;

final class DateTimeImmutableNormalizer implements CustomObjectNormalizerInterface
{
    public function normalize(object $object): array
    {
        if ( ! ($object instanceof \DateTimeImmutable)) {
            throw new InvalidObjectTypeException($object::class, \DateTimeImmutable::class);
        }

        return ['value' => $object->format(DateTimeInterface::ATOM)];
    }

    public function denormalize(array $data): object
    {
        return new \DateTimeImmutable($data['value']);
    }

    public function getObjectClass(): string
    {
        return \DateTimeImmutable::class;
    }
}
