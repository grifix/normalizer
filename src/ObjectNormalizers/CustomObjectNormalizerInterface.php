<?php

declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers;

interface CustomObjectNormalizerInterface
{
    public function normalize(object $object): array;

    public function denormalize(array $data): object;

    public function getObjectClass(): string;
}
