<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Exceptions;

final class VersionMetaDataNotFoundException extends \Exception
{

    public function __construct()
    {
        parent::__construct('Version meta data not found in the data array!');
    }
}
