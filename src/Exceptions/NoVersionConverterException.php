<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Exceptions;

use Exception;

final class NoVersionConverterException extends Exception
{
    public function __construct(string $normalizerName)
    {
        parent::__construct(sprintf('Version converter for normalizer [%s] must be defined!', $normalizerName));
    }
}
