<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Exceptions;

use Exception;

final class EmptySchemasException extends Exception
{
    public function __construct(string $normalizerName)
    {
        parent::__construct(sprintf('Schemas are empty for normalizer [%s]!', $normalizerName));
    }
}
