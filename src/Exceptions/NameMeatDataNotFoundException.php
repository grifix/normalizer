<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Exceptions;

final class NameMeatDataNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Name meta data not found in the data array!');
    }
}
