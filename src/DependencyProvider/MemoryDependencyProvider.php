<?php
declare(strict_types=1);

namespace Grifix\Normalizer\DependencyProvider;

final class MemoryDependencyProvider implements DependencyProviderInterface
{

    /** @var object[] */
    private array $dependencies = [];

    public function get(string $class): object
    {
        return $this->dependencies[$class];
    }

    public function set(object $object): void
    {
        $this->dependencies[$object::class] = $object;
    }
}
