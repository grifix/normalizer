<?php
declare(strict_types=1);

namespace Grifix\Normalizer\DependencyProvider;

interface DependencyProviderInterface
{
    public function get(string $class): object;

    public function set(object $object): void;
}
