<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Tests;

use DateTime;
use Grifix\Normalizer\Exceptions\EmptySchemasException;
use Grifix\Normalizer\Exceptions\NameMeatDataNotFoundException;
use Grifix\Normalizer\Exceptions\NoVersionConverterException;
use Grifix\Normalizer\Exceptions\VersionMetaDataNotFoundException;
use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\DateTimeNormalizer;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\Exceptions\InvalidValueClassException;
use Grifix\Normalizer\Repository\Exceptions\NormalizerAlreadyExistException;
use Grifix\Normalizer\Repository\Exceptions\NormalizerDoesNotExistException;
use Grifix\Normalizer\SchemaValidator\Exceptions\InvalidDataException;
use Grifix\Normalizer\SchemaValidator\Repository\Exceptions\SchemaAlreadyExistsException;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Normalizer\Tests\Dummies\Car;
use Grifix\Normalizer\Tests\Dummies\Circle;
use Grifix\Normalizer\Tests\Dummies\Container;
use Grifix\Normalizer\Tests\Dummies\Engine;
use Grifix\Normalizer\Tests\Dummies\Rectangle;
use Grifix\Normalizer\Tests\Dummies\Repair;
use Grifix\Normalizer\Tests\Dummies\Report;
use Grifix\Normalizer\Tests\Dummies\Services\CarService;
use Grifix\Normalizer\Tests\Dummies\Services\EngineService;
use Grifix\Normalizer\Tests\Dummies\Square;
use Grifix\Normalizer\Tests\Dummies\TechnicalInspection;
use Grifix\Normalizer\Tests\Dummies\Unknown;
use Grifix\Normalizer\VersionConverter\Exceptions\UnsupportedVersionException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;
use PHPUnit\Framework\TestCase;

final class NormalizerTest extends TestCase
{
    private Normalizer $normalizer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->normalizer = Normalizer::create();
        $this->registerCarNormalizer($this->normalizer);
        $this->registerEngineNormalizer($this->normalizer);
        $this->registerRepairNormalizer($this->normalizer);
        $this->registerTechnicalInspectionNormalizer($this->normalizer);
        $this->registerDateTimeNormalizer($this->normalizer);
        $this->normalizer->registerDependency(new CarService());
        $this->normalizer->registerDependency(new EngineService());
    }

    private function registerCarNormalizer(Normalizer $normalizer): void
    {
        $normalizer->registerDefaultObjectNormalizer(
            'car',
            Car::class,
            [
                Schema::create()
                    ->withStringProperty('model')
                    ->withArrayOfObjectsProperty('repairs', ['repair'])
                    ->withArrayOfObjectsProperty('technicalInspections', ['technical-inspection'])
                    ->withObjectProperty('engine', ['engine'])
            ],
            null,
            ['service']
        );
    }

    private function registerEngineNormalizer(Normalizer $normalizer): void
    {
        $normalizer->registerDefaultObjectNormalizer(
            'engine',
            Engine::class,
            [
                Schema::create()
                    ->withStringProperty('type')
                    ->withNumberProperty('volume')
            ],
            null,
            ['service']
        );
    }

    private function registerRepairNormalizer(Normalizer $normalizer): void
    {
        $normalizer->registerDefaultObjectNormalizer(
            'repair',
            Repair::class,
            [
                Schema::create()
                    ->withStringProperty('note')
            ],
        );
    }

    private function registerTechnicalInspectionNormalizer(Normalizer $normalizer): void
    {
        $normalizer->registerDefaultObjectNormalizer(
            'technical-inspection',
            TechnicalInspection::class,
            [
                Schema::create()
                    ->withObjectProperty('date', ['date-time'])
                    ->withArrayProperty('notes')
            ],
        );
    }

    private function registerDateTimeNormalizer(Normalizer $normalizer): void
    {
        $normalizer->registerCustomObjectNormalizer(
            'date-time',
            new DateTimeNormalizer(),
            [
                Schema::create()->withStringProperty('value')
            ],
        );
    }

    /**
     * @dataProvider normalizerDataProvider
     */
    public function testItNormalizes(object $object, array $expectedResult): void
    {
        self::assertEquals($expectedResult, $this->normalizer->normalize($object));
    }

    /**
     * @dataProvider normalizerDataProvider
     */
    public function testItDenormalizes(object $expectedResult, array $data): void
    {
        $this->normalizer->normalize($expectedResult); //to create json schemas automatically
        self::assertEquals($expectedResult, $this->normalizer->denormalize($data));
    }

    public function normalizerDataProvider(): array
    {
        return [
            [
                new DateTime('2001-01-01T00:00:00+00:00'),
                [
                    '__normalizer__' => [
                        'name' => 'date-time',
                        'version' => 1
                    ],
                    'value' => '2001-01-01T00:00:00+00:00'
                ]
            ],
            [
                new Car(
                    service: new CarService(),
                    model: 'Audi',
                    repairs: [],
                    technicalInspections: []
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car',
                        'version' => 1
                    ],
                    'model' => 'Audi',
                    'engine' => null,
                    'repairs' => [],
                    'technicalInspections' => []
                ]
            ],
            [
                new Car(
                    service: new CarService(),
                    model: 'Audi',
                    repairs: [],
                    technicalInspections: [],
                    engine: new Engine(
                        new EngineService(),
                        'gasoline',
                        5.1
                    )
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car',
                        'version' => 1

                    ],
                    'model' => 'Audi',
                    'engine' => [
                        '__normalizer__' => [
                            'name' => 'engine',
                            'version' => 1
                        ],
                        'type' => 'gasoline',
                        'volume' => 5.1
                    ],
                    'repairs' => [],
                    'technicalInspections' => []
                ]
            ],
            [
                new Car(
                    service: new CarService(),
                    model: 'Audi',
                    repairs: [
                        new Repair(
                            note: 'Engine'
                        ),
                        new Repair(
                            note: 'Door'
                        )
                    ],
                    technicalInspections: []
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car',
                        'version' => 1
                    ],
                    'model' => 'Audi',
                    'engine' => null,
                    'repairs' => [
                        [
                            '__normalizer__' => [
                                'name' => 'repair',
                                'version' => 1
                            ],
                            'note' => 'Engine'
                        ],
                        [
                            '__normalizer__' => [
                                'name' => 'repair',
                                'version' => 1
                            ],
                            'note' => 'Door'
                        ]
                    ],
                    'technicalInspections' => []
                ]
            ],
            [
                new Car(
                    service: new CarService(),
                    model: 'Audi',
                    repairs: [
                        new Repair(
                            note: 'Engine'
                        ),
                        new Repair(
                            note: 'Door'
                        )
                    ],
                    technicalInspections: [
                        new TechnicalInspection(
                            date: new DateTime('2020-01-01'),
                            notes: [
                                'one',
                                'two',
                                'three'
                            ]
                        )
                    ]
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car',
                        'version' => 1
                    ],
                    'model' => 'Audi',
                    'engine' => null,
                    'repairs' => [
                        [
                            '__normalizer__' => [
                                'name' => 'repair',
                                'version' => 1

                            ],
                            'note' => 'Engine'
                        ],
                        [
                            '__normalizer__' => [
                                'name' => 'repair',
                                'version' => 1
                            ],
                            'note' => 'Door'
                        ]
                    ],
                    'technicalInspections' => [
                        [
                            '__normalizer__' => [
                                'name' => 'technical-inspection',
                                'version' => 1
                            ],
                            'notes' => [
                                'one',
                                'two',
                                'three'
                            ],
                            'date' => [
                                '__normalizer__' => [
                                    'name' => 'date-time',
                                    'version' => 1
                                ],
                                'value' => '2020-01-01T00:00:00+00:00'
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @dataProvider invalidDataProvider
     */
    public function testItCannotDenormalize(
        array $data,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $this->normalizer->denormalize($data);
    }

    public function invalidDataProvider(): array
    {
        return [
            [
                [

                ],
                NameMeatDataNotFoundException::class,
                'Name meta data not found in the data array!'
            ],
            [
                [
                    '__normalizer__' => ''
                ],
                NameMeatDataNotFoundException::class,
                'Name meta data not found in the data array!'
            ],
            [
                [
                    '__normalizer__' => []
                ],
                NameMeatDataNotFoundException::class,
                'Name meta data not found in the data array!'
            ],
            [
                [
                    '__normalizer__' => [
                        'name' => 'car'
                    ]
                ],
                VersionMetaDataNotFoundException::class,
                'Version meta data not found in the data array!'
            ]
        ];
    }

    public function testItCannotFindDenormalizer(): void
    {
        $this->expectException(NormalizerDoesNotExistException::class);
        $this->expectExceptionMessage('Normalizer with name [unknown] does not exist!');
        $this->normalizer->denormalize([
            'name' => 'Joe',
            '__normalizer__' => [
                'name' => 'unknown',
            ],
        ]);
    }

    public function testItCannotNormalize(): void
    {
        $this->expectException(NormalizerDoesNotExistException::class);
        $this->expectExceptionMessage(
            'Normalizer with object class [Grifix\Normalizer\Tests\Dummies\Unknown] does not exist!'
        );
        $this->normalizer->normalize(new Unknown());
    }

    public function testItDoesntAddNormalizerWithSameName(): void
    {
        $this->expectException(SchemaAlreadyExistsException::class);
        $this->expectExceptionMessage('Schema already exists for normalizer [car] in version [1]!');
        $this->normalizer->registerDefaultObjectNormalizer(
            'car',
            Unknown::class,
            [
                Schema::create()
            ]
        );
    }

    public function testItDoesntAddNormalizerWithSameClass(): void
    {
        $this->expectException(NormalizerAlreadyExistException::class);
        $this->expectExceptionMessage(
            'Normalizer with object class [Grifix\Normalizer\Tests\Dummies\Car] already exists!'
        );
        $this->normalizer->registerDefaultObjectNormalizer(
            'new',
            Car::class,
            [
                Schema::create()
            ]
        );
    }

    public function testItCannotDenormalizeWithNotExistingData(): void
    {
        $normalizer = Normalizer::create();
        $normalizer->registerDefaultObjectNormalizer(
            'repair',
            Repair::class,
            [
                Schema::create()->withStringProperty('note')
            ]

        );
        $this->expectException(InvalidDataException::class);
        $this->expectDeprecationMessage(
            'Invalid data for normalizer [repair] version [1]!'
        );
        $normalizer->denormalize(
            [
                'foo' => 'bar',
                '__normalizer__' => [
                    'name' => 'repair',
                    'version' => 1
                ]
            ]
        );
    }

    public function testItConvertsData(): void
    {
        $normalizer = Normalizer::create();
        $normalizer->registerDefaultObjectNormalizer(
            'repair',
            Repair::class,
            [
                Schema::create()->withStringProperty('text'),
                Schema::create()->withStringProperty('description'),
                Schema::create()->withStringProperty('note'),
            ],

            new class implements VersionConverterInterface {
                public function convert(array $data, int $dataVersion, string $normalizerName): array
                {
                    switch ($dataVersion) {
                        case 1:
                            $data['description'] = $data['text'];
                            unset($data['text']);
                            break;
                        case 2:
                            $data['note'] = $data['description'];
                            unset($data['description']);
                            break;
                        default:
                            throw new UnsupportedVersionException(
                                $normalizerName,
                                $dataVersion
                            );
                    }
                    return $data;
                }
            }
        );

        self::assertEquals(
            new Repair('test'),
            $normalizer->denormalize([
                'text' => 'test',
                '__normalizer__' => [
                    'version' => 1,
                    'name' => 'repair'
                ]
            ])
        );

        self::assertEquals(
            new Repair('test'),
            $normalizer->denormalize([
                'description' => 'test',
                '__normalizer__' => [
                    'version' => 2,
                    'name' => 'repair'
                ]
            ])
        );

        self::assertEquals(
            new Repair('test'),
            $normalizer->denormalize([
                'note' => 'test',
                '__normalizer__' => [
                    'version' => 3,
                    'name' => 'repair'
                ]
            ])
        );
    }

    public function testItDoesNotValidateInvalidData(): void
    {
        $this->expectException(InvalidDataException::class);
        $this->expectExceptionMessage('Invalid data for normalizer [repair] version [1]!');
        $this->normalizer->denormalize([
            'value' => 'test',
            '__normalizer__' => [
                'name' => 'repair',
                'version' => 1
            ]
        ]);
    }

    public function testItDoesNotRegisterNormalizerWithoutJsonSchemas(): void
    {
        $normalizer = Normalizer::create();
        $this->expectException(EmptySchemasException::class);
        $this->expectExceptionMessage('Schemas are empty for normalizer [repair]!');
        $normalizer->registerDefaultObjectNormalizer(
            'repair',
            Repair::class,
            []
        );
    }

    public function testItDoesNotRegisterNormalizerWithoutVersionConverter(): void
    {
        $normalizer = Normalizer::create();
        $this->expectException(NoVersionConverterException::class);
        $this->expectExceptionMessage('Version converter for normalizer [repair] must be defined!');
        $normalizer->registerDefaultObjectNormalizer(
            'repair',
            Repair::class,
            [
                Schema::create(),
                Schema::create()
            ]
        );
    }

    public function testItWorksWithCustomNormalizer(): void
    {
        $normalizer = Normalizer::create();

        $normalizer->registerCustomObjectNormalizer(
            'date-time',
            $this->createCustomNormalizer(),
            [
                Schema::create()->withStringProperty('date'),
                Schema::create()->withStringProperty('val'),
                Schema::create()->withStringProperty('value'),
            ],
            $this->createCustomVersionConverter()
        );

        $date = new DateTime('2020-01-28T16:22:37-07:00');
        $data = $normalizer->normalize($date);
        self::assertEquals([
            'value' => '2020-01-28T16:22:37-07:00',
            '__normalizer__' => [
                'version' => 3,
                'name' => 'date-time'
            ]
        ], $data);
        self::assertEquals($date, $normalizer->denormalize($data));
        self::assertEquals(
            $date,
            $normalizer->denormalize([
                'val' => '2020-01-28T16:22:37-07:00',
                '__normalizer__' => [
                    'version' => 2,
                    'name' => 'date-time'
                ]
            ])
        );
    }

    public function testItHasNormalizerWithName(): void
    {
        self::assertTrue($this->normalizer->hasNormalizerWithName('car'));
        self::assertFalse($this->normalizer->hasNormalizerWithName('car2'));
    }

    public function testItHasNormalizerWithObjectClass(): void
    {
        self::assertTrue($this->normalizer->hasNormalizerWithObjectClass(Car::class));
        self::assertFalse($this->normalizer->hasNormalizerWithObjectClass(Unknown::class));
    }

    public function testItDoesNotNormalizeWithNotExistedNestedNormalizer(): void
    {
        $normalizer = Normalizer::create();
        $this->registerDateTimeNormalizer($normalizer);
        $normalizer->registerDefaultObjectNormalizer(
            'report',
            Report::class,
            [
                Schema::create()
                    ->withObjectProperty('date', ['date-type'], true)
                    ->withArrayOfObjectsProperty('repairs', ['unknown'])
            ]
        );

        $this->expectException(NormalizerDoesNotExistException::class);
        $this->expectExceptionMessage('Normalizer with name [unknown] does not exist!');
        $normalizer->normalize(new Report());
    }

    public function testItDoesNotNormalizerWithWrongNestedArrayValueType(): void
    {
        $normalizer = Normalizer::create();
        $this->registerDateTimeNormalizer($normalizer);
        $this->registerRepairNormalizer($normalizer);
        $normalizer->registerDefaultObjectNormalizer(
            'report',
            Report::class,
            [
                Schema::create()
                    ->withObjectProperty('date', ['date-time'])
                    ->withArrayOfObjectsProperty('repairs', ['repair'])
                ,
            ],
        );
        $this->expectException(InvalidValueClassException::class);
        $this->expectExceptionMessage(
            'Invalid value type for property [Grifix\Normalizer\Tests\Dummies\Report::repairs]! Allowed types are [Grifix\Normalizer\Tests\Dummies\Repair] but [Grifix\Normalizer\Tests\Dummies\Unknown] given!'
        );
        $normalizer->normalize(new Report(null, [new Unknown()]));
    }

    public function testItDoesntNormalizeWithAdditionalProperties(): void
    {
        $this->normalizer->registerDefaultObjectNormalizer(
            'report',
            Report::class,
            [
                Schema::create()->withArrayOfObjectsProperty('repairs', ['repair'])
            ]
        );
        $this->expectException(InvalidDataException::class);
        $this->normalizer->normalize(new Report());
    }

    public function testItDoesNotNormalizerWithWrongNestedObjectValueType(): void
    {
        $normalizer = Normalizer::create();
        $normalizer->registerCustomObjectNormalizer(
            'unknown',
            new class implements CustomObjectNormalizerInterface {

                public function normalize(object $object): array
                {
                    return [];
                }

                public function denormalize(array $data): object
                {
                    return new Unknown();
                }

                public function getObjectClass(): string
                {
                    return Unknown::class;
                }
            },
            [
                Schema::create()
            ]
        );
        $this->registerRepairNormalizer($normalizer);
        $normalizer->registerDefaultObjectNormalizer(
            'report',
            Report::class,
            [
                Schema::create()
                    ->withObjectProperty('date', ['unknown'], true)
                    ->withArrayOfObjectsProperty('repairs', ['repair'])
            ],
        );
        $this->expectException(InvalidValueClassException::class);
        $this->expectExceptionMessage(
            'Invalid value type for property [Grifix\Normalizer\Tests\Dummies\Report::date]! Allowed types are [Grifix\Normalizer\Tests\Dummies\Unknown] but [DateTime] given!'
        );
        $normalizer->normalize(new Report(new DateTime(), [new Repair('test')]));
    }

    public function testItWorksWithMixedObjects(): void
    {
        $normalizer = Normalizer::create();
        $this->registerRepairNormalizer($normalizer);
        $this->registerDateTimeNormalizer($normalizer);
        $normalizer->registerDefaultObjectNormalizer(
            'report',
            Report::class,
            [
                Schema::create()
                    ->withMixedObjectProperty('date')
                    ->withArrayProperty('repairs')
            ]
        );

        $object = new Report(
            new DateTime('2021-01-01'),
            [
                new Repair('test'),
                new Repair('test2')
            ]
        );

        $expectedData = [
            'date' =>
                [
                    'value' => '2021-01-01T00:00:00+00:00',
                    '__normalizer__' =>
                        [
                            'name' => 'date-time',
                            'version' => 1,
                        ],
                ],
            'repairs' =>
                [
                    0 =>
                        [
                            'note' => 'test',
                            '__normalizer__' =>
                                [
                                    'name' => 'repair',
                                    'version' => 1,
                                ],
                        ],
                    1 =>
                        [
                            'note' => 'test2',
                            '__normalizer__' =>
                                [
                                    'name' => 'repair',
                                    'version' => 1,
                                ],
                        ],
                ],
            '__normalizer__' =>
                [
                    'name' => 'report',
                    'version' => 1,
                ],
        ];
        self::assertEquals($expectedData, $normalizer->normalize($object));
        self::assertEquals($object, $normalizer->denormalize($expectedData));
    }

    private function createCustomNormalizer(): CustomObjectNormalizerInterface
    {
        return new class () implements CustomObjectNormalizerInterface {

            public function normalize(object $object): array
            {
                /** @var $object DateTime */
                return [
                    'value' => $object->format(\DateTimeInterface::ATOM)
                ];
            }

            public function denormalize(array $data): object
            {
                return new DateTime($data['value']);
            }

            public function getObjectClass(): string
            {
                return DateTime::class;
            }
        };
    }

    private function createCustomVersionConverter(): VersionConverterInterface
    {
        return new class implements VersionConverterInterface {

            public function convert(array $data, int $dataVersion, string $normalizerName): array
            {
                switch ($dataVersion) {
                    case 1:
                        $data['val'] = $data['date'];
                        unset($data['date']);
                        break;
                    case 2:
                        $data['value'] = $data['val'];
                        unset($data['val']);
                        break;
                    default:
                        throw new UnsupportedVersionException(
                            $normalizerName,
                            $dataVersion
                        );
                }
                return $data;
            }
        };
    }

    private function createContainerNormalizer(): Normalizer
    {
        $normalizer = Normalizer::create();
        $normalizer->registerDefaultObjectNormalizer(
            'square',
            Square::class,
            [
                Schema::create()->withStringProperty('name')
            ]
        );
        $normalizer->registerDefaultObjectNormalizer(
            'rectangle',
            Rectangle::class,
            [
                Schema::create()->withStringProperty('name')
            ]
        );
        $normalizer->registerDefaultObjectNormalizer(
            'circle',
            Circle::class,
            [
                Schema::create()->withStringProperty('name')
            ]
        );

        $normalizer->registerDefaultObjectNormalizer(
            'container',
            Container::class,
            [
                Schema::create()
                    ->withObjectProperty(
                        'element',
                        ['square', 'circle']
                    )
                    ->withArrayOfObjectsProperty(
                        'elements',
                        ['square', 'circle']
                    )
            ]
        );
        return $normalizer;
    }

    public function testItNormalizerMultiTypes(): void
    {
        $normalizer = $this->createContainerNormalizer();

        $container = new Container(
            new Square('square'),
            [
                new Square('1'),
                new Circle('2')
            ]
        );

        $data = $normalizer->normalize($container);
        self::assertEquals(
            [
                'element' =>
                    [
                        'name' => 'square',
                        '__normalizer__' =>
                            [
                                'name' => 'square',
                                'version' => 1,
                            ],
                    ],
                'elements' =>
                    [
                        0 =>
                            [
                                'name' => '1',
                                '__normalizer__' =>
                                    [
                                        'name' => 'square',
                                        'version' => 1,
                                    ],
                            ],
                        1 =>
                            [
                                'name' => '2',
                                '__normalizer__' =>
                                    [
                                        'name' => 'circle',
                                        'version' => 1,
                                    ],
                            ],
                    ],
                '__normalizer__' =>
                    [
                        'name' => 'container',
                        'version' => 1,
                    ],
            ],
            $data
        );

        self::assertEquals($container, $normalizer->denormalize($data));
    }

    public function testItDoesNotNormalizeWithWrongTypeInArray(): void
    {
        $normalizer = $this->createContainerNormalizer();
        $this->expectException(InvalidValueClassException::class);
        $this->expectExceptionMessage(
            'Invalid value type for property [Grifix\Normalizer\Tests\Dummies\Container::elements]! Allowed types are [Grifix\Normalizer\Tests\Dummies\Square, Grifix\Normalizer\Tests\Dummies\Circle] but [Grifix\Normalizer\Tests\Dummies\Rectangle] given!'
        );
        $normalizer->normalize(
            new Container(null, [
                new Circle('1'),
                new Square('2'),
                new Rectangle('3')
            ])
        );
    }

    public function testItNormalizerAssociativeArray(): void
    {
        $normalizer = $this->createContainerNormalizer();
        $container = new Container(
            null,
            [
                'a' => new Circle('1'),
                'b' => new Circle('2')
            ]
        );
        $data = $normalizer->normalize($container);
        self::assertEquals($container, $normalizer->denormalize($data));
    }
}
