<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Dummies;


use Grifix\Normalizer\Tests\Dummies\Services\CarService;

class Car
{

    /**
     * @param Repair[] $repairs
     * @param TechnicalInspection[] $technicalInspections
     */
    public function __construct(
        public readonly CarService $service,
        public readonly string $model,
        public readonly array $repairs,
        public readonly array $technicalInspections,
        public readonly ?Engine $engine = null
    ) {
    }

}
