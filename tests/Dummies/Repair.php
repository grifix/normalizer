<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Dummies;


class Repair
{
    public function __construct(public readonly string $note)
    {
    }

}
