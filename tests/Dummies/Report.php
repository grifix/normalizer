<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Dummies;

final class Report
{

    /**
     * @param Repair[] $repairs
     */
    public function __construct(
        public readonly ?\DateTime $date = null,
        public readonly array $repairs = [],
    ) {
    }
}
