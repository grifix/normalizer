<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Dummies;

final class Container
{

    /**
     * @param Square|Circle|null $element
     * @param Square[]|Circle[] $elements
     */
    public function __construct(
        public Square|Circle|null $element,
        public array $elements = []
    ) {
    }
}
