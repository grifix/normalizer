<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Dummies;


use Grifix\Normalizer\Tests\Dummies\Services\EngineService;

class Engine
{
    public function __construct(
        public readonly EngineService $service,
        public readonly string $type,
        public readonly float $volume
    )
    {
    }
}
