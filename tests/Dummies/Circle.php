<?php

declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Dummies;

final class Circle
{

    public function __construct(public string $name)
    {
    }
}
